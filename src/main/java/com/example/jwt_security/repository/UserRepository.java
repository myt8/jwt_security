package com.example.jwt_security.repository;

import com.example.jwt_security.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Integer> {

    Optional<User> findByFirstname(String firstname);

    Optional<User> findByEmail(String email);
}
