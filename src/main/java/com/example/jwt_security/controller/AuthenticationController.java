package com.example.jwt_security.controller;

import com.example.jwt_security.request.AuthenticationRequest;
import com.example.jwt_security.request.RegisterRequest;
import com.example.jwt_security.response.AuthenticationResponse;
import com.example.jwt_security.service.AuthenticationService;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;

@RestController
@RequestMapping("/auth")
@RequiredArgsConstructor
public class AuthenticationController {

    private final AuthenticationService authenticationService;

    /**
     * Register: Đăng ký tài khoản người dùng
     *
     * @param request Yêu cầu người dùng gửi lên để đăng ký tài khoản
     * @return trả về thông tin người dùng và token
     */
    @PostMapping("/register")
    public ResponseEntity<AuthenticationResponse> register(@RequestBody RegisterRequest request) {
        return ResponseEntity.ok(authenticationService.register(request));
    }

    /**
     * Start Login: Đăng nhập vào tài khoản vừa đăng ký
     * Hàm này xác thực xem user này có tồn tại trong hộ thống chưa
     *
     * @param request thông tin xác thực gồm Username(Or Email) và Password
     */
    @PostMapping("/authenticate")
    public ResponseEntity<AuthenticationResponse> authenticate(@RequestBody AuthenticationRequest request) {
        return ResponseEntity.ok(authenticationService.authenticate(request));
    }

    @PostMapping("/refresh-token")
    public void refreshToken(HttpServletRequest request, HttpServletResponse response) throws IOException{
        authenticationService.refreshToken(request, response);
    }
}
